#ifndef HFWDETRPCTRB3LOOKUP_H
#define HFWDETRPCTRB3LOOKUP_H

#include "TObjArray.h"
#include "hparset.h"
#include "htrbnetdef.h"

#define FWDETRPC_TRB3_LOOKUP_SIZE 512

using namespace Trbnet;
using namespace std;

class HFwDetRpcTrb3LookupChan : public TObject
{
protected:
    Char_t  module;       // module number
    Char_t  layer;        // layer number
    Char_t  strip;        // strip number
    Char_t  lrside;       // L/R side: L==0, R==1

public:
    HFwDetRpcTrb3LookupChan()  { clear(); }
    virtual ~HFwDetRpcTrb3LookupChan() {}

    Char_t getModule()  {return module;}
    Char_t getLayer()   {return layer;}
    Char_t getStrip()   {return strip;}
    Char_t getLRside()  {return lrside;}

    void getAddress(Char_t& m, Char_t& l, Char_t& s, Char_t& lrs) const
    {
        m = module;
        l = layer;
        s = strip;
        lrs = lrside;
    }

    void clear()
    {
        module = -1;
        layer = -1;
        strip = -1;
        lrside = -1;
    }

    void setModule(Char_t m)    { module = m; }
    void setLayer(Char_t l)     { layer = l; }
    void setStrip(Char_t s)     { strip = s; }
    void setLRside(Char_t lrs)  { lrside = lrs; }

    void fill(Char_t m, Char_t l, Char_t s, Char_t lrs)
    {
        module = m;
        layer = l;
        strip = s;
        lrside = lrs;
    }

    void fill(HFwDetRpcTrb3LookupChan& r)
    {
        module = r.getModule();
        layer = r.getLayer();
        strip = r.getStrip();
        lrside = r.getLRside();
    }

    ClassDef(HFwDetRpcTrb3LookupChan, 1) // Channel level of the lookup table for the FwDet Rpc TRB3 unpacker
};


class HFwDetRpcTrb3LookupTdc: public TObject {
    friend class HFwDetRpcTrb3Lookup;
protected:
    TObjArray* array;     // pointer array containing HFwDetRpcTrb3LookupChan objects
public:
    HFwDetRpcTrb3LookupTdc();
    ~HFwDetRpcTrb3LookupTdc();

    HFwDetRpcTrb3LookupChan* getChannel(Int_t c) {
        if (c >= 0 && c < FWDETRPC_TRB3_LOOKUP_SIZE) return &((*this)[c]);
        else return 0;
    }

    HFwDetRpcTrb3LookupChan& operator[](Int_t i) {
        return *static_cast<HFwDetRpcTrb3LookupChan*>((*array)[i]);
    }

    Int_t getSize()  {return FWDETRPC_TRB3_LOOKUP_SIZE;}
    void  clear();
    ClassDef(HFwDetRpcTrb3LookupTdc, 1) // Board level of the lookup table for the FwDet Rpc TRB3 unpacker
};


class HFwDetRpcTrb3Lookup : public HParSet {
protected:
    TObjArray* array;  // array of pointers of type HFwDetRpcTrb3LookupTdc
    Int_t arrayOffset; // offset to calculate the index
public:
    HFwDetRpcTrb3Lookup(const Char_t* name = "FwDetRpcTrb3Lookup",
                          const Char_t* title = "Lookup table for the TRB3 unpacker of the FwDet Rpc",
                          const Char_t* context = "FwDetRpcTrb3LookupProduction",
                          Int_t minTrbnetAddress = Trbnet::kFwDetRpcTrb3MinTrbnetAddress,
                          Int_t maxTrbnetAddress = Trbnet::kFwDetRpcTrb3MaxTrbnetAddress);
    ~HFwDetRpcTrb3Lookup();

    HFwDetRpcTrb3LookupTdc* getTdc(Int_t trbnetAddress) {
        if ((arrayOffset <= trbnetAddress) && (trbnetAddress <= (array->GetSize() + arrayOffset)))
            return (HFwDetRpcTrb3LookupTdc*)(array->At(trbnetAddress - arrayOffset));
        return 0;
    }
    HFwDetRpcTrb3LookupTdc* operator[](Int_t i) {
        return static_cast<HFwDetRpcTrb3LookupTdc*>((*array)[i]);
    }

    Int_t getSize() {return array->GetLast() + 1;}
    Int_t getArrayOffset() {return arrayOffset;}

    Bool_t init(HParIo* input, Int_t* set);
    Int_t  write(HParIo* output);
    void   clear();
    void   printParams();
    Bool_t fill(Int_t, Int_t, Char_t, Char_t, Char_t, Char_t);
    Bool_t readline(const Char_t*);
    void   putAsciiHeader(TString&);
    void   write(fstream&);
    ClassDef(HFwDetRpcTrb3Lookup, 1) // Lookup table for the TRB3 unpacker of the FwDet Rpc
};

#endif  /*!HFWDETRPCTRB3LOOKUP_H*/
