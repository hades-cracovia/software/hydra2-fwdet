#ifndef HFWDETRPCRAW_H
#define HFWDETRPCRAW_H

#include "hades.h"
#include "fwdetdef.h"

#include "TObject.h"

class HFwDetRpcRaw : public TObject
{
protected:
    Char_t  fModule;    // module number (0..1 for Rpc modules)
    Char_t  fLayer; // geant cell number
    Char_t  fStrip;
    Float_t fTimeL[FWDET_RPC_MAX_HITS];     // time left
    Float_t fTimeR[FWDET_RPC_MAX_HITS];     // time right
    Float_t fChargeL[FWDET_RPC_MAX_HITS];   // charge left
    Float_t fChargeR[FWDET_RPC_MAX_HITS];   // charge right
    Int_t   nHitsNumL;
    Int_t   nHitsNumR;

public:
    HFwDetRpcRaw();
    ~HFwDetRpcRaw();

    void   getAddress(Char_t& m, Char_t& l, Char_t& s) const;
    Int_t  getHitsNumL() const { return nHitsNumL; }
    Int_t  getHitsNumR() const { return nHitsNumR; }

    Bool_t getTimeAndWidthL(Int_t n, Float_t & t, Float_t & q) const;
    Bool_t getTimeAndWidthR(Int_t n, Float_t & t, Float_t & q) const;

    void   setAddress(Char_t m, Char_t l, Char_t s);

    Bool_t setTimeAndWidthL(Int_t n, Float_t t, Float_t q);
    Bool_t setTimeAndWidthR(Int_t n, Float_t t, Float_t q);

    Bool_t addTimeAndWidthL(Float_t t, Float_t q);
    Bool_t addTimeAndWidthR(Float_t t, Float_t q);

    void print() const;

private:

    ClassDef(HFwDetRpcRaw, 1)
};

#endif /* !HFWDETRPCRAW_H */
