//_HADES_CLASS_DESCRIPTION
///////////////////////////////////////////////////////////////////////
//
//  HFwDetStrawTrb3Lookup
//
//  Lookup table for the TRB3 unpacker of the FwDetStraw to map the
//  Trbnet address (range defined in htrbnetdef.h) and channel (0..255)
//  to the detector address module, cell.
//
///////////////////////////////////////////////////////////////////////

#include "hfwdetstrawtrb3lookup.h"
#include "hdetpario.h"
#include "hpario.h"
#include <fstream>

using namespace std;

ClassImp(HFwDetStrawTrb3LookupChan)
ClassImp(HFwDetStrawTrb3LookupTdc)
ClassImp(HFwDetStrawTrb3Lookup)

HFwDetStrawTrb3LookupTdc::HFwDetStrawTrb3LookupTdc()
{
    // constructor creates an array of FWDETSTRAW_TRB3_LOOKUP_SIZE channels
    array = new TObjArray(FWDETSTRAW_TRB3_LOOKUP_SIZE);
    for (Int_t i = 0; i < FWDETSTRAW_TRB3_LOOKUP_SIZE; i++) array->AddAt(new HFwDetStrawTrb3LookupChan(), i);
}

HFwDetStrawTrb3LookupTdc::~HFwDetStrawTrb3LookupTdc()
{
    // destructor deletes the array of channels
    array->Delete();
    delete array;
}

void HFwDetStrawTrb3LookupTdc::clear()
{
    // calls the clear function for each channel
    for (Int_t i = 0; i < FWDETSTRAW_TRB3_LOOKUP_SIZE; i++)(*this)[i].clear();
}

//---------------------------------------------------------------

HFwDetStrawTrb3Lookup::HFwDetStrawTrb3Lookup(const Char_t* name,
                                             const Char_t* title,
                                             const Char_t* context,
                                             Int_t minTrbnetAddress,
                                             Int_t maxTrbnetAddress)
: HParSet(name, title, context)
{
    // constructor creates an empty array of size nBoards
    arrayOffset = minTrbnetAddress;
    array = new TObjArray(maxTrbnetAddress - minTrbnetAddress + 1);
}

HFwDetStrawTrb3Lookup::~HFwDetStrawTrb3Lookup()
{
    // destructor
    array->Delete();
    delete array;
}

Bool_t HFwDetStrawTrb3Lookup::init(HParIo* inp, Int_t* set)
{
    // initializes the container from an input
    HDetParIo* input = inp->getDetParIo("HFwDetParIo");
    if (input) return (input->init(this, set));
    return kFALSE;
}

Int_t HFwDetStrawTrb3Lookup::write(HParIo* output)
{
    // writes the container to an output
    HDetParIo* out = output->getDetParIo("HFwDetParIo");
    if (out) return out->write(this);
    return -1;
}

void HFwDetStrawTrb3Lookup::clear()
{
    // deletes all HFwDetStrawTrb3LookupTdc objects from the array and resets the input versions
    array->Delete();
    status = kFALSE;
    resetInputVersions();
}

void HFwDetStrawTrb3Lookup::printParams()
{
    // prints the lookup table
    printf("Lookup table for the TRB3 unpacker of the FwDetStraw\n");
    printf("trbnet-address  channel  ->  module  layer  cell  subcell\n");
    for (Int_t i = 0; i <= array->GetLast(); i++) {
        HFwDetStrawTrb3LookupTdc* b = (*this)[i];
        if (b)
        {
            for (Int_t j = 0; j < b->getSize(); j++)
            {
                HFwDetStrawTrb3LookupChan& chan = (*b)[j];
                Int_t module = chan.getModule();
                if (module >= 0)
                {
                    printf("0x%x %4i %5i %5i %5i %5i\n",
                           arrayOffset + i, j, module, chan.getLayer(), chan.getCell(), chan.getSubCell());
                }
            }
        }
    }
}

Bool_t HFwDetStrawTrb3Lookup::fill(Int_t id, Int_t chan,
                                   Char_t mod, Char_t layer, Int_t cell, Char_t subcell)
{
    // creates the HFwDetStrawTrb3LookupTdc objects, if not existing, and fills the channel
    Bool_t rc = kFALSE;
    HFwDetStrawTrb3LookupTdc* p = getTdc(id);
    if (!p)
    {
        p = new HFwDetStrawTrb3LookupTdc();
        array->AddAt(p, id - arrayOffset);
    }
    HFwDetStrawTrb3LookupChan* c = p->getChannel(chan);
    if (c)
    {
        c->fill(mod, layer, cell, subcell);
        rc = kTRUE;
    }
    else
    {
        Error("fill", "Invalid channel number %i", chan);
    }
    return rc;
}

Bool_t HFwDetStrawTrb3Lookup::readline(const Char_t *buf)
{
    // decodes one line read from ASCII file I/O and fills the channel
    Bool_t rc = kFALSE;
    Int_t id, chan, mod, lay, cell, subcell;
    Int_t n = sscanf(buf, " 0x%x %i %i %i %i %i", &id, &chan, &mod, &lay, &cell, &subcell);
    if (6 == n)
    {
        rc = fill(id, chan, mod, lay, cell, subcell);
    }
    else
    {
        if (n < 6) Error("readline", "Not enough values in line %s\n", buf);
        else     Error("readline", "Too many values in line %s\n", buf);
    }
    return rc;
}

void HFwDetStrawTrb3Lookup::putAsciiHeader(TString& header)
{
    // puts the ASCII header to the string used in HFwDetStrawParAsciiFileIo
    header = "# Lookup table for the TRB3 unpacker of the FwDetStraw\n"
    "# Format:\n"
    "# trbnet-address  channel  module  layer  cell  subcell\n";
}

void HFwDetStrawTrb3Lookup::write(fstream& fout)
{
    // writes the information of all non-zero HFwDetStrawTrb3LookupTdc objects to the ASCII file
    for (Int_t i = 0; i <= array->GetLast(); i++)
    {
        HFwDetStrawTrb3LookupTdc* b = (*this)[i];
        if (b)
        {
            for (Int_t j = 0; j < b->getSize(); j++)
            {
                HFwDetStrawTrb3LookupChan& chan = (*b)[j];
                Int_t module = chan.getModule();
                if (module >= 0)
                {
                    fout << "0x" << hex << (arrayOffset + i) << dec << setw(5) << j
                    << setw(5) << module << setw(5) << chan.getLayer()
                    << setw(5) << chan.getCell() << setw(5) << chan.getSubCell() << '\n';
                }
            }
        }
    }
}
