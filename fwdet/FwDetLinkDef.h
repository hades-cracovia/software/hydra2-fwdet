#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class HFwDetDetector;
#pragma link C++ class HFwDetContFact;
#pragma link C++ class HFwDetParRootFileIo;
#pragma link C++ class HFwDetParAsciiFileIo;
#pragma link C++ class HFwDetGeomPar;
#pragma link C++ class HFwDetTaskSet;

#pragma link C++ class HFwDetStrawGeomPar;
#pragma link C++ class HFwDetStrawTrb3Lookup;
#pragma link C++ class HFwDetStrawTrb3LookupTdc;
#pragma link C++ class HFwDetStrawTrb3LookupChan;
#pragma link C++ class HFwDetStrawTrb3Calpar;
#pragma link C++ class HFwDetStrawTrb3Unpacker;
#pragma link C++ class HFwDetStrawRaw;
#pragma link C++ class HFwDetStrawStatusPar;
#pragma link C++ class HFwDetStrawCalibrater;
#pragma link C++ class HFwDetStrawCal;
#pragma link C++ class HFwDetStrawCalSim;
#pragma link C++ class HFwDetStrawCalRunPar;
#pragma link C++ class HFwDetStrawDigitizer;
#pragma link C++ class HFwDetStrawDigiPar;
#pragma link C++ class HFwDetStrawDigitizer;
#pragma link C++ class HFwDetStrawCalSim;

#pragma link C++ class HFwDetRpcGeomPar;
#pragma link C++ class HFwDetRpcTrb3Lookup;
#pragma link C++ class HFwDetRpcTrb3LookupTdc;
#pragma link C++ class HFwDetRpcTrb3LookupChan;
#pragma link C++ class HFwDetRpcTrb3Calpar;
#pragma link C++ class HFwDetRpcTrb3Unpacker;
#pragma link C++ class HFwDetRpcRaw;
#pragma link C++ class HFwDetRpcCal;
#pragma link C++ class HFwDetRpcCalSim;
#pragma link C++ class HFwDetRpcDigitizer;
#pragma link C++ class HFwDetRpcDigiPar;
#pragma link C++ class HFwDetRpcHit;
#pragma link C++ class HFwDetRpcHitFinder;
#pragma link C++ class HFwDetRpcHitFinderPar;

#pragma link C++ class HFwDetCand-;
#pragma link C++ class HFwDetCandSim-;
#pragma link C++ class HFwDetVectorFinder;
#pragma link C++ class HFwDetVectorFinderPar;

#pragma link C++ global catFwDetStrawRaw;
#pragma link C++ global catFwDetStrawCal;
#pragma link C++ global catFwDetRpcRaw;
#pragma link C++ global catFwDetRpcCal;
#pragma link C++ global catFwDetRpcHit;
#pragma link C++ global catFwDetCand;

#endif
