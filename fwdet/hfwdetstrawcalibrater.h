#ifndef HFWDETSTRAWCALIBRATER_H
#define HFWDETSTRAWCALIBRATER_H

#include "hreconstructor.h"
#include "hfwdetstrawstatuspar.h"

class HCategory;
class HIterator;
class HFwDetStrawGeomPar;
class HFwDetStrawStatusPar;
class HFwDetStrawCalRunPar;

class HFwDetStrawCalibrater : public HReconstructor
{
protected:
    HCategory *pRawCat;          // pointer to the raw data
    HCategory *pCalCat;          // pointer to the cal data
    HIterator *iter;             // iterator on raw data.
    HLocation loc;               // location for new cal object

    HFwDetStrawGeomPar * pGeomPar; //! strips geometry
    HFwDetStrawStatusPar * pStatusPar; //! straw status
    HFwDetStrawCalRunPar * pCalRunPar; //! cal run par

public:
    HFwDetStrawCalibrater(void);
    HFwDetStrawCalibrater(const Text_t* name, const Text_t* title);
    ~HFwDetStrawCalibrater(void);
    Bool_t init(void);
    Bool_t reinit(void);
    Int_t  execute(void);
    Bool_t finalize(void) {return kTRUE;}

private:
    Int_t straw_in_layer_offset[FWDET_STRAW_MAX_MODULES][ FWDET_STRAW_MAX_LAYERS];
    Int_t straws_in_layer[FWDET_STRAW_MAX_MODULES][ FWDET_STRAW_MAX_LAYERS];
    Int_t short_straw_offset[FWDET_STRAW_MAX_MODULES][FWDET_STRAW_MAX_LAYERS];

    Int_t calcStrawIndex(Int_t mod, Int_t lay, Int_t straw, Int_t subcell);

    ClassDef(HFwDetStrawCalibrater, 0) // Calibrater raw->cal for PionTracker data
};

#endif /* !HFWDETSTRAWCALIBRATER_H */
