//*-- AUTHOR : Ilse Koenig
//*-- Created : 16/11/2015
//*-- Modified : Rafał Lalik

//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////
//
//  HFwDetContFact
//
//  Factory for the parameter containers in libFwDet
//
/////////////////////////////////////////////////////////////

#include "hfwdetcontfact.h"
#include "hruntimedb.h"
#include "hfwdetgeompar.h"
#include "hfwdetstrawgeompar.h"
#include "hfwdetstrawdigipar.h"
#include "hfwdetstrawcalrunpar.h"
#include "hfwdetrpcgeompar.h"
#include "hfwdetrpcdigipar.h"
#include "hfwdetrpchitfinderpar.h"
#include "hfwdetvectorfinderpar.h"

#include "hfwdetstrawtrb3lookup.h"
#include "hfwdetrpctrb3lookup.h"

#include "hfwdetstrawtrb3calpar.h"
#include "hfwdetrpctrb3calpar.h"

#include "hfwdetstrawstatuspar.h"

ClassImp(HFwDetContFact);

static HFwDetContFact gHFwDetContFact;

HFwDetContFact::HFwDetContFact()
{
    // Constructor (called when the library is loaded)
    fName = "FwDetContFact";
    fTitle = "Factory for parameter containers in libFwDet";
    setAllContainers();
    HRuntimeDb::instance()->addContFactory(this);
}

HFwDetContFact::~HFwDetContFact()
{
}

void HFwDetContFact::setAllContainers()
{
    // Creates the Container objects with all accepted contexts and adds them to
    // the list of containers for the FwDet library.
    containers->Add(
        new HContainer("FwDetGeomPar",
            "Geometry parameters of the Forward Detector",
            "GeomProduction"));
    containers->Add(
        new HContainer("FwDetStrawGeomPar",
            "Geometry parameters of the Straw Forward Detector",
            "StrawGeomProduction"));
    containers->Add(
        new HContainer("FwDetRpcGeomPar",
            "Geometry parameters of the RPC Forward Detector",
            "RpcGeomProduction"));

    containers->Add(
        new HContainer("FwDetStrawDigiPar",
            "FwDetStraw digitization parameters",
            "FwDetStrawDigiProduction"));
    containers->Add(
        new HContainer("FwDetRpcDigiPar",
            "FwDetRpc digitization parameters",
            "FwDetRpcDigiProduction"));

    containers->Add(
        new HContainer("FwDetStrawCalRunPar",
            "FwDetStraw CalRun parameters",
            "FwDetStrawCalRunProduction"));

    containers->Add(
        new HContainer("FwDetRpcHitFinderPar",
            "FwDetRpc digitization parameters",
            "FwDetRpcDigiProduction"));

    containers->Add(
        new HContainer("FwDetVectorFinderPar",
            "FwDetStraw vector finder parameters",
            "FwDetStrawVectorFinderProduction"));

    containers->Add(
        new HContainer("FwDetStrawTrb3Lookup",
            "Lookup table for the TRB3 unpacker of the FwDet-Straw",
            "FwDetStrawTrb3LookupProduction"));

    containers->Add(
        new HContainer("FwDetStrawTrb3Calpar",
            "TRB3 TDC calibration parameters of the FwDet-Straw detector",
            "FwDetStrawTrb3CalparProduction"));

    containers->Add(
        new HContainer("FwDetRpcTrb3Lookup",
            "Lookup table for the TRB3 unpacker of the FwDet-Rpc",
            "FwDetRpcTrb3LookupProduction"));

    containers->Add(
        new HContainer("FwDetRpcTrb3Calpar",
            "TRB3 TDC calibration parameters of the FwDet-Rpc detector",
            "FwDetRpcTrb3CalparProduction"));

    containers->Add(
        new HContainer("FwDetStrawStatusPar",
            "FwDetStraw Straw status parameters",
            "FwDetStrawStatusParProduction"));
}

HParSet* HFwDetContFact::createContainer(HContainer* c)
{
    // Calls the constructor of the corresponding parameter container.
    // For an actual context, which is not an empty string and not the default context
    // of this container, the name is concatinated with the context.
    const Char_t* name=c->GetName();
    if (strcmp(name,"FwDetGeomPar") == 0)
        return new HFwDetGeomPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());
    if (strcmp(name,"FwDetStrawGeomPar") == 0)
        return new HFwDetStrawGeomPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());
    if (strcmp(name,"FwDetRpcGeomPar") == 0)
        return new HFwDetRpcGeomPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetStrawDigiPar") == 0)
        return new HFwDetStrawDigiPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());
    if (strcmp(name,"FwDetRpcDigiPar") == 0)
        return new HFwDetRpcDigiPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetStrawCalRunPar") == 0)
        return new HFwDetStrawCalRunPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetRpcHitFinderPar") == 0)
        return new HFwDetRpcHitFinderPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetVectorFinderPar") == 0)
        return new HFwDetVectorFinderPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetStrawTrb3Lookup") == 0)
        return new HFwDetStrawTrb3Lookup(c->getConcatName().Data(),c->GetTitle(),c->getContext());
    if (strcmp(name,"FwDetRpcTrb3Lookup") == 0)
        return new HFwDetRpcTrb3Lookup(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetStrawTrb3Calpar") == 0)
        return new HFwDetStrawTrb3Calpar(c->getConcatName().Data(),c->GetTitle(),c->getContext());
    if (strcmp(name,"FwDetRpcTrb3Calpar") == 0)
        return new HFwDetRpcTrb3Calpar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    if (strcmp(name,"FwDetStrawStatusPar") == 0)
        return new HFwDetStrawStatusPar(c->getConcatName().Data(),c->GetTitle(),c->getContext());

    return 0;
}
