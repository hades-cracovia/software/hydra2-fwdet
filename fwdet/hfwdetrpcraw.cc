//*-- Author  : Rafał Lalik
//*-- Created : 01.06.2016

//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////
//  HFwDetRpcRawSim
//
//  This class contains Forward Rpc detector Raw data
//
/////////////////////////////////////////////////////////////

#include "hfwdetrpcraw.h"

ClassImp(HFwDetRpcRaw)

HFwDetRpcRaw::HFwDetRpcRaw()
{
//     fTrack = -1;
    nHitsNumL = 0;
    nHitsNumR = 0;
    fModule = fLayer = -1;
    for (Int_t i = 0; i < FWDET_RPC_MAX_HITS; ++i)
    {
        fTimeL[i] = fTimeR[i] = -100000.F;
        fChargeL[i] = fChargeR[i] = -100000.F;
    }
}

HFwDetRpcRaw::~HFwDetRpcRaw()
{
}

void HFwDetRpcRaw::getAddress(Char_t& m, Char_t& l, Char_t& s) const
{
    m = fModule;
    l = fLayer;
    s = fStrip;
}

void HFwDetRpcRaw::setAddress(Char_t m, Char_t l, Char_t s)
{
    fModule = m;
    fLayer = l;
    fStrip = s;
}

Bool_t HFwDetRpcRaw::getTimeAndWidthL(Int_t n, Float_t& t, Float_t& q) const
{
    if (n < nHitsNumL)
    {
        t = fTimeL[n];
        q = fChargeL[n];
        return true;
    }
    else
    {
        t = -100000.F;
        q = -100000.F;
        return false;
    }
}

Bool_t HFwDetRpcRaw::getTimeAndWidthR(Int_t n, Float_t& t, Float_t& q) const
{
    if (n < nHitsNumR)
    {
        t = fTimeR[n];
        q = fChargeR[n];
        return true;
    }
    else
    {
        t = -100000.F;
        q = -100000.F;
        return false;
    }
}

Bool_t HFwDetRpcRaw::setTimeAndWidthL(Int_t n, Float_t t, Float_t q)
{
    if (n >= 0 && n < nHitsNumL)
    {
        fTimeL[n] = t;
        fChargeL[n] = q;

        return true;
    }

    return false;
}

Bool_t HFwDetRpcRaw::setTimeAndWidthR(Int_t n, Float_t t, Float_t q)
{
    if (n >= 0 && n < nHitsNumR)
    {
        fTimeR[n] = t;
        fChargeR[n] = q;

        return true;
    }

    return false;
}

Bool_t HFwDetRpcRaw::addTimeAndWidthL(Float_t t, Float_t q)
{
    if (nHitsNumL < FWDET_RPC_MAX_HITS)
    {
        fTimeL[nHitsNumL] = t;
        fChargeL[nHitsNumL] = q;

        ++nHitsNumL;
        return true;
    }

    return false;
}

Bool_t HFwDetRpcRaw::addTimeAndWidthR(Float_t t, Float_t q)
{
    if (nHitsNumR < FWDET_RPC_MAX_HITS)
    {
        fTimeR[nHitsNumR] = t;
        fChargeR[nHitsNumR] = q;

        ++nHitsNumR;
        return true;
    }

    return false;
}

void HFwDetRpcRaw::print() const
{
    printf(" RPC hit: m=%d l=%d s=%d:\n", fModule, fLayer, fStrip);
    printf("  left :");
    for (Int_t i = 0; i < nHitsNumL; ++i)
    {
        printf("  hit %d : t=%f q=%f  ",
               i, fTimeL[i], fChargeL[i]);
    }
    printf("\n  right:");
    for (Int_t i = 0; i < nHitsNumR; ++i)
    {
        printf("  hit %d : t=%f q=%f  ",
               i, fTimeR[i], fChargeR[i]);
    }
    printf("\n");
}
