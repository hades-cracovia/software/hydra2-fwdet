//*-- Author  : Georgy Kornakov
//*-- Created : 27.01.2016

//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////
//  HFwDetStrawDigiGeomPar
//
//  Container class of Forward Straw detector Digi par
//
/////////////////////////////////////////////////////////////

#include "hfwdetstrawstatuspar.h"
#include "hparamlist.h"
#include "hpario.h"

ClassImp(HFwDetStrawStatusPar);

const size_t field_width = 5;
HFwDetStrawStatusPar::HFwDetStrawStatusPar(const Char_t* name, const Char_t* title,
                    const Char_t* context) : HParCond(name, title, context),
                    nStraws(0)
{
    clear();
}

HFwDetStrawStatusPar::~HFwDetStrawStatusPar()
{
}

void HFwDetStrawStatusPar::clear()
{
//     nStraws = 0;
//     nStrawStatusM.Set(0);
//     nStrawStatusL.Set(0);
//     nStrawStatusS.Set(0);
//     nStrawStatusC.Set(0);
//     nStrawStatus.Set(0);
    const size_t n = FWDET_STRAW_MAX_MODULES*FWDET_STRAW_MAX_LAYERS*FWDET_STRAW_MAX_STRAWS*FWDET_STRAW_MAX_SUBCELLS;
    memset(feb_status, 0, n*sizeof(Int_t));

//     status = kFALSE;
//     resetInputVersions();
//     changed = kFALSE;
}

void HFwDetStrawStatusPar::putParams(HParamList* l)
{
    // puts all parameters to the parameter list, which is used by the io
    if (!l) return;
    l->add("nStraws", nStraws);
    TArrayI feb_status_arr(nStraws*field_width);
    for (Int_t i = 0; i < nStraws; ++i)
    {
        feb_status_arr[i*field_width + 0] = nStrawStatusM[i];
        feb_status_arr[i*field_width + 1] = nStrawStatusL[i];
        feb_status_arr[i*field_width + 2] = nStrawStatusS[i];
        feb_status_arr[i*field_width + 3] = nStrawStatusC[i];
        feb_status_arr[i*field_width + 4] = nStrawStatus[i];
    }
    l->add("nStrawStatus", feb_status_arr);
}

Bool_t HFwDetStrawStatusPar::getParams(HParamList* l)
{
    // gets all parameters from the parameter list, which is used by the io
    if (!l) return kFALSE;
    if (!( l->fill("nStraws", &nStraws) )) return kFALSE;

    TArrayI feb_status_arr(nStraws*field_width);
    if (!( l->fill("nStrawStatus", &feb_status_arr) )) return kFALSE;
    if (feb_status_arr.GetSize() != nStraws*(int)field_width)
    {
        Error("HFwDetStrawStatusPar::getParams(HParamList* l)",
              "Array size of nStrawStatus=%d does not fit to number of febs=%d", feb_status_arr.GetSize()/(int)field_width, (int)nStraws);
        return kFALSE;
    }

    setStraws(nStraws);

    for (Int_t i = 0; i < nStraws; ++i)
    {
        nStrawStatusM[i] = feb_status_arr[i*field_width + 0];
        nStrawStatusL[i] = feb_status_arr[i*field_width + 1];
        nStrawStatusS[i] = feb_status_arr[i*field_width + 2];
        nStrawStatusC[i] = feb_status_arr[i*field_width + 3];
        nStrawStatus[i]  = feb_status_arr[i*field_width + 4];

        Int_t m = nStrawStatusM[i];
        Int_t l = nStrawStatusL[i];
        Int_t s = nStrawStatusS[i];
        Int_t c = nStrawStatusC[i];
        feb_status[(int)m][(int)l][s][(int)c] = 1+i;
    }

    return kTRUE;
}

Char_t HFwDetStrawStatusPar::getStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c)
{
    Int_t source = feb_status[(int)m][(int)l][s][(int)c];
    if (source == 0)
        return 1;

    return nStrawStatus[source-1];
}

Bool_t HFwDetStrawStatusPar::setStraws(Int_t n)
{
    nStraws = n;
    nStrawStatusM.Set(n);
    nStrawStatusL.Set(n);
    nStrawStatusS.Set(n);
    nStrawStatusC.Set(n);
    nStrawStatus.Set(n);

    return kTRUE;
}

Bool_t HFwDetStrawStatusPar::setStrawStatus(Int_t n, Char_t status)
{
    if (n < nStraws)
    {
        nStrawStatus[n] = status;

        return kTRUE;
    }

    return kFALSE;
}

Bool_t HFwDetStrawStatusPar::setStrawStatus(Int_t n, Char_t m, Char_t l, Int_t s, Char_t c, Char_t status)
{
    if (n < nStraws)
    {
        // get feb old address
        Int_t old_m = nStrawStatusM[n];
        Int_t old_l = nStrawStatusL[n];
        Int_t old_s = nStrawStatusS[n];
        Int_t old_c = nStrawStatusC[n];

        Bool_t same_addr = (old_m == m) && (old_l == l) && (old_s == s) && (old_c == c);
        // check if it had sorce in params and if yes, change this
        // but only if the new address is different that the old one
        Int_t source = feb_status[old_m][old_l][old_s][old_c]; // old index
        if ( !same_addr && source > 0 )
        {
            feb_status[old_m][old_l][old_s][old_c] = 0;
            feb_status[(int)m][(int)l][s][(int)c] = n+1;
        }

        nStrawStatusM[n] = m;
        nStrawStatusL[n] = l;
        nStrawStatusS[n] = s;
        nStrawStatusC[n] = c;
        nStrawStatus[n] = status;

        return kTRUE;
    }

    return kFALSE;
}

Int_t HFwDetStrawStatusPar::addStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c, Char_t status)
{
    Int_t source = feb_status[(int)m][(int)l][s][(int)c];

    // if it already exists, overwrite old one only
    if (source > 0)
    {
        nStrawStatus[source-1] = status;
        return nStraws;
    }

    ++nStraws;
    nStrawStatusM.Set(nStraws);
    nStrawStatusL.Set(nStraws);
    nStrawStatusS.Set(nStraws);
    nStrawStatusC.Set(nStraws);
    nStrawStatus.Set(nStraws);

    nStrawStatusM[nStraws-1] = m;
    nStrawStatusL[nStraws-1] = l;
    nStrawStatusS[nStraws-1] = s;
    nStrawStatusC[nStraws-1] = c;
    nStrawStatus[nStraws-1] = status;

    // mark status as custom
    feb_status[(int)m][(int)l][s][(int)c] = nStraws;

    return nStraws;
}

Int_t HFwDetStrawStatusPar::delStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c)
{
    Int_t source = feb_status[(int)m][(int)l][s][(int)c];

    // remove it if it exists in custom array
    if (source > 0)
    {
        return delStrawStatus(source-1);
    }

    return nStraws;
}

Int_t HFwDetStrawStatusPar::delStrawStatus(Int_t n)
{
    // mark status as custom
    Int_t m = nStrawStatusM[n];
    Int_t l = nStrawStatusL[n];
    Int_t s = nStrawStatusS[n];
    Int_t c = nStrawStatusC[n];
    feb_status[(int)m][(int)l][s][(int)c] = 0;

    Int_t idx = 0;
    for(Int_t i = 0; i < nStraws; ++i)
    {
        if (i == n)
            continue;

        nStrawStatusM[idx] = nStrawStatusM[i];
        nStrawStatusL[idx] = nStrawStatusL[i];
        nStrawStatusS[idx] = nStrawStatusS[i];
        nStrawStatusC[idx] = nStrawStatusC[i];
        nStrawStatus[idx]  = nStrawStatus[i];

        ++idx;
    }

    --nStraws;
    nStrawStatusM.Set(nStraws);
    nStrawStatusL.Set(nStraws);
    nStrawStatusS.Set(nStraws);
    nStrawStatusC.Set(nStraws);
    nStrawStatus.Set(nStraws);

    return nStraws;
}

void HFwDetStrawStatusPar::printParams()
{
    std::cout<<"\n---------------------------------------------\n";
    std::cout<<"-----  "<<GetName()<<"  -----\n";
    if (!paramContext.IsNull()) std::cout<<"--  Context/Purpose:  "<<paramContext<<'\n';
    if (!author.IsNull())       std::cout<<"--  Author:           "<<author<<'\n';
    if (!description.IsNull())  std::cout<<"--  Description:      "<<description<<'\n';
    std::cout<<"---------------------------------------------\n";
    printf(" %d straw defined\n", nStraws);
    for (Int_t i = 0; i < nStraws; ++i)
    {
        printf(" [%3d] m=%1d   l=%1d   s=%3d   c=%1d   status=%d\n", i,
               (int)nStrawStatusM[i], (int)nStrawStatusL[i],
               nStrawStatusS[i], (int)nStrawStatusC[i], (int)nStrawStatus[i]);
    }
    std::cout<<"---------------------------------------------\n\n";
}
