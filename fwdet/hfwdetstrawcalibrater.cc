//_HADES_CLASS_DESCRIPTION
////////////////////////////////////////////////////////////////
//
//  HFwDetStrawCalibrater:
//
//  Calibrates all fired cells in PionTrackerRaw category and fills
//  the PionTrackerCal category
//
////////////////////////////////////////////////////////////////

#include "hfwdetstrawcalibrater.h"
#include "fwdetdef.h"
#include "hfwdetstrawraw.h"
#include "hfwdetstrawcal.h"
#include "hfwdetdetector.h"
#include "hfwdetstrawgeompar.h"
#include "hfwdetstrawstatuspar.h"
#include "hfwdetstrawcalrunpar.h"
#include "hades.h"
#include "hcategory.h"
#include "hdebug.h"
#include "hevent.h"
#include "hiterator.h"
#include "hruntimedb.h"
#include "hrun.h"
#include "hspectrometer.h"

#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;

// raw category, cal category, iterator, cal parameter, run_id
ClassImp(HFwDetStrawCalibrater)

HFwDetStrawCalibrater::HFwDetStrawCalibrater(void) :
    pRawCat(NULL), pCalCat(NULL), iter(NULL), pStatusPar(NULL)
{
    // default constructor
}

HFwDetStrawCalibrater::HFwDetStrawCalibrater(const Text_t *name, const Text_t *title) : HReconstructor(name, title),
    pRawCat(NULL), pCalCat(NULL), iter(NULL), pStatusPar(NULL)
{
    // constructor
}

HFwDetStrawCalibrater::~HFwDetStrawCalibrater(void)
{
    //destructor deletes the iterator on the raw category
    if (NULL != iter)
    {
        delete iter;
        iter = NULL;
    }
}

Bool_t HFwDetStrawCalibrater::init(void)
{
    HFwDetDetector* det = (HFwDetDetector*)gHades->getSetup()->getDetector("FwDet");
    if (!det)
    {
        Error("init", "No FwDet found.");
        return kFALSE;
    }

//     pFebsPar = (HFwDetStrawFebsPar*)gHades->getRuntimeDb()->getContainer("FwDetStrawFebsPar");
//     if (!pFebsPar) return kFALSE;

    pGeomPar = (HFwDetStrawGeomPar*)gHades->getRuntimeDb()->getContainer("FwDetStrawGeomPar");
    if (!pGeomPar) return kFALSE;

    pStatusPar = (HFwDetStrawStatusPar*)gHades->getRuntimeDb()->getContainer("FwDetStrawStatusPar");
    if (!pStatusPar) return kFALSE;

    pCalRunPar = (HFwDetStrawCalRunPar*)gHades->getRuntimeDb()->getContainer("FwDetStrawCalRunPar");
    if (!pCalRunPar) return kFALSE;

    pRawCat = gHades->getCurrentEvent()->getCategory(catFwDetStrawRaw);
    if (!pRawCat)
    {
        Error("init()", "HFwDetStrawRaw category not available!");
        return kFALSE;
    }

    pCalCat = det->buildCategory(catFwDetStrawCal);
    if (!pCalCat) return kFALSE;

    iter = (HIterator*)pRawCat->MakeIterator();
    loc.set(5, 0, 0);
    fActive = kTRUE;

    return kTRUE;
}

Bool_t HFwDetStrawCalibrater::reinit(void)
{
#if DEBUG_LEVEL > 2
    pGeomPar->printParams();
    pStatusPar->printParams();
    pCalRunPar->printParams();

    printf("Init offset matrices for straws\n");
#endif
    
    Int_t mods = pGeomPar->getModules();
    Int_t int_straw = 0;
    for (Int_t i = 0; i < mods; ++i)
    {
        Int_t lays = pGeomPar->getLayers(i);
        Int_t straws = pGeomPar->getStraws(i)*2;
        Int_t shorti = pGeomPar->getShortIndex(i)*2;
        Int_t shortw = pGeomPar->getShortWidth(i)*2;
        for (Int_t j = 0; j < lays; ++j)
        {
            short_straw_offset[i][j] = shorti;
            straws_in_layer[i][j] = straws;
            straw_in_layer_offset[i][j] = int_straw;
            int_straw += straws + shortw;
        }
    }

    return kTRUE;
}

Int_t HFwDetStrawCalibrater::execute(void)
{
    // calibrates all fired cells [strip,module]
    HFwDetStrawRaw *pRaw = NULL;
    HFwDetStrawCal *pCal = NULL;

    Char_t mod     = 0;
    Char_t lay     = 0;
    Char_t plane   = 0;
    Int_t straw    = 0;
    Char_t subcell = 0;
    Int_t nhits    = 0;   // TODO to be removed?
    Int_t idx = 0;

    Float_t rawTime     = 0.F;
    Float_t rawWidth    = 0.F;
    Float_t calTime     = 0.F;
    Float_t calCharge   = 0.F;
    Float_t calX        = 0;
    Float_t calZ        = 0;

    //Fill cal category
    iter->Reset();
    while ((pRaw = (HFwDetStrawRaw *)iter->Next()) != 0) //interation over fired cells
    {
      //cout << "*****************\n";
        pRaw->getAddress(mod, lay, straw, subcell);
        plane = straw % 2;
        straw /= 2;
        loc[0] = mod;
        loc[1] = lay;
        loc[2] = plane;
        loc[3] = straw;
        loc[4] = subcell;

        Int_t straw_status = pStatusPar->getStrawStatus(mod, lay, straw, subcell);
        if (straw_status == 0)
            continue;

        if (mod >= 0)
        {
            pCal = (HFwDetStrawCal*)pCalCat->getObject(loc);
            if (!pCal) // should be no object under location loc yet
            {
                pCal = (HFwDetStrawCal *)pCalCat->getSlot(loc);
                if (pCal) // there should be space reserved for the slot
                {
                    pCal = new(pCal) HFwDetStrawCal;
                    pCal->setAddress(mod, lay, plane, straw, subcell);
                }
                else
                {
                    Error("execute()", "Can't get slot mod=%i, lay=%i, plane=%d, straw=%d, subcell=%d", mod, lay, plane, straw, subcell);
                    return -1;
                }
            }
            else
            {
                Error("execute()", "Slot already exists for mod=%i, lay=%i, plane=%d, straw=%d, subcell=%d", mod, lay, plane, straw, subcell);
                return -1;

            }

            // loop over number of hits, still in while, mod & strip already selected
//             nhits = pRaw->getMultiplicity();
//             nhits = 1;  // we store only one hit
//             for (Int_t i = 0; i < nhits; ++i)
            {
                // get data from raw category
                pRaw->getTimeAndWidth(rawTime, rawWidth);

                idx = calcStrawIndex(mod, lay, straw, subcell);
                // correct the time and width
                calTime   = rawTime - pCalRunPar->getOffset(idx);

                calCharge = rawWidth;

                calX = pGeomPar->getOffsetX(mod, lay, plane)
                + straw*pGeomPar->getStrawPitch(mod);
                calZ = pGeomPar->getOffsetZ(mod, lay, plane);

                // set calibrated data
                pCal->setHit(calTime, calCharge, calX, calZ);
            }
        }
    }
    //cout << "End event\n";
    return 0;
}

Int_t HFwDetStrawCalibrater::calcStrawIndex(Int_t mod, Int_t lay, Int_t straw, Int_t subcell)
{
    Int_t straw_idx = straw_in_layer_offset[mod][lay] + straw;
    if ((subcell & 0x2) == subcell)  // lower straw
        straw_idx += (straws_in_layer[mod][lay] - short_straw_offset[mod][lay]);

    return straw_idx;
}
