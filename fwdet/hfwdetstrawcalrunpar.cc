//*-- Author  : Rafał Lalik
//*-- Created : 22.03.2018

//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////
//  HFwDetStrawCalRunPar
//
//  Container class of Forward Straw detector cal run par
//
/////////////////////////////////////////////////////////////

#include "hfwdetstrawcalrunpar.h"
#include "hparamlist.h"

ClassImp(HFwDetStrawCalRunPar);

HFwDetStrawCalRunPar::HFwDetStrawCalRunPar(const Char_t* name, const Char_t* title,
                    const Char_t* context) : HParCond(name, title, context)
{
    clear();
}

HFwDetStrawCalRunPar::~HFwDetStrawCalRunPar()
{
}

void HFwDetStrawCalRunPar::clear()
{
    nDataNum = 0;
    fOffset = 0.0;
}

void HFwDetStrawCalRunPar::putParams(HParamList* l)
{
    // puts all parameters to the parameter list, which is used by the io
    if (!l) return;
    l->add("nDataNum",       nDataNum);
    l->add("fOffset",        fOffset);
}

Bool_t HFwDetStrawCalRunPar::getParams(HParamList* l)
{
    // gets all parameters from the parameter list, which is used by the io
    if (!l) return kFALSE;

    Int_t par_modules;
    if (!l->fill("nDataNum", &nDataNum))
        return kFALSE;

    if (!l->fill("fOffset", &fOffset))
        return kFALSE;

    if (fOffset.GetSize() != nDataNum)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of offsets %d does not fit to number of data %d", fOffset.GetSize(), nDataNum);
        return kFALSE;
    }

    return kTRUE;
}
