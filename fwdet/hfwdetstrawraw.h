#ifndef HFWDETSTRAWRAW_H
#define HFWDETSTRAWRAW_H

#include "TObject.h"

class HFwDetStrawRaw : public TObject
{
private:
    Char_t  fModule;    // module number (0 and 1 for Straw modules)
    Char_t  fLayer;     // layer number (0 - 3)
    Int_t   fStraw;     // straw number in the plane
    Char_t  fUpDown;    // up-down straw
    Float_t fTime;      // hit detection time (tof + drift_time - start_offset)
    Float_t fWidth;     // width
    Int_t   fMultiplicity; // hits multiplicity in the straw (only first is stored)

public:
    HFwDetStrawRaw();
    virtual ~HFwDetStrawRaw();

    inline void  getAddress(Char_t& m, Char_t& l, Int_t& s, Char_t& ud) const;
    inline void  getTimeAndWidth(Float_t& time, Float_t& adc) const;
    Int_t getModule() const { return fModule; }
    Int_t getLayer() const { return fLayer; }
    Int_t getStraw() const { return fStraw; }
    Float_t getTime() const { return fTime; }
    Float_t getWidth() const { return fWidth; }
    Int_t getUDconf() const { return fUpDown; }
    Int_t getMultiplicity() const { return fMultiplicity; }

    inline void  setAddress(Char_t m, Char_t l, Int_t s, Char_t ud);
    inline void  setTimeAndWidth(Float_t time, Float_t width);

    ClassDef(HFwDetStrawRaw, 1);
};

void HFwDetStrawRaw::getAddress(Char_t &m, Char_t &l, Int_t &s, Char_t& ud) const
{
    m  = fModule;
    l  = fLayer;
    s  = fStraw;
    ud = fUpDown;
}

void HFwDetStrawRaw::setAddress(Char_t m, Char_t l, Int_t s, Char_t ud)
{
    fModule = m;
    fLayer  = l;
    fStraw  = s;
    fUpDown = ud;
}

void HFwDetStrawRaw::getTimeAndWidth(Float_t& time, Float_t& width) const
{
    time  = fTime;
    width = fWidth;
}

void HFwDetStrawRaw::setTimeAndWidth(Float_t time, Float_t width)
{
    ++fMultiplicity;
    if (fMultiplicity > 1)
        return;

    fTime  = time;
    fWidth = width;
}

#endif /* ! HFWDETSTRAWRAW_H */
