//*-- Author  : Rafał Lalik
//*-- Created : 01.03.2018

//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////
//  HFwDetStrawRaw
//
//  This class contains Forward Straw detector raw data
//
/////////////////////////////////////////////////////////////

#include "hfwdetstrawraw.h"
#include "fwdetdef.h"

ClassImp(HFwDetStrawRaw);

HFwDetStrawRaw::HFwDetStrawRaw() : fModule(-1), fLayer(-1), fStraw(-1), fUpDown(0),
        fTime(-100000.0), fWidth(-100000.0), fMultiplicity(0)
{
}

HFwDetStrawRaw::~HFwDetStrawRaw()
{
}
