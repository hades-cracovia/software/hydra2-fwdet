#ifndef HFWDETSTRAWTRB3LOOKUP_H
#define HFWDETSTRAWTRB3LOOKUP_H

#include "TObjArray.h"
#include "hparset.h"
#include "htrbnetdef.h"

#define FWDETSTRAW_TRB3_LOOKUP_SIZE 512

using namespace Trbnet;
using namespace std;

class HFwDetStrawTrb3LookupChan : public TObject
{
protected:
    Char_t  module;       // module number
    Char_t  layer;        // layer number
    Int_t   cell;         // cell number
    Char_t  subcell;      // sub-cell number

public:
    HFwDetStrawTrb3LookupChan()  { clear(); }
    virtual ~HFwDetStrawTrb3LookupChan() {}

    Char_t getModule()  {return module;}
    Char_t getLayer()   {return layer;}
    Int_t getCell()     {return cell;}
    Char_t getSubCell() {return subcell;}

    void getAddress(Char_t& m, Char_t& l, Int_t& c, Char_t& s) const
    {
        m = module;
        l = layer;
        c = cell;
        s = subcell;
    }

    void clear()
    {
        module = -1;
        layer = -1;
        cell = -1;
        subcell = 0;
    }

    void setModule(Char_t m)    { module = m; }
    void setLayer(Char_t l)     { layer = l; }
    void setCell(Int_t c)       { cell = c; }
    void setSubCell(Char_t s)   { subcell = s; }

    void fill(Char_t m, Char_t l, Int_t c, Char_t s)
    {
        module = m;
        layer = l;
        cell = c;
        subcell = s;
    }

    void fill(HFwDetStrawTrb3LookupChan& r)
    {
        module = r.getModule();
        layer = r.getLayer();
        cell = r.getCell();
        subcell = r.getSubCell();
    }

    ClassDef(HFwDetStrawTrb3LookupChan, 1) // Channel level of the lookup table for the FwDet Straw TRB3 unpacker
};


class HFwDetStrawTrb3LookupTdc: public TObject {
    friend class HFwDetStrawTrb3Lookup;
protected:
    TObjArray* array;     // pointer array containing HFwDetStrawTrb3LookupChan objects
public:
    HFwDetStrawTrb3LookupTdc();
    ~HFwDetStrawTrb3LookupTdc();

    HFwDetStrawTrb3LookupChan* getChannel(Int_t c) {
        if (c >= 0 && c < FWDETSTRAW_TRB3_LOOKUP_SIZE) return &((*this)[c]);
        else return 0;
    }

    HFwDetStrawTrb3LookupChan& operator[](Int_t i) {
        return *static_cast<HFwDetStrawTrb3LookupChan*>((*array)[i]);
    }

    Int_t getSize()  {return FWDETSTRAW_TRB3_LOOKUP_SIZE;}
    void  clear();
    ClassDef(HFwDetStrawTrb3LookupTdc, 1) // Board level of the lookup table for the FwDet Straw TRB3 unpacker
};


class HFwDetStrawTrb3Lookup : public HParSet {
protected:
    TObjArray* array;  // array of pointers of type HFwDetStrawTrb3LookupTdc
    Int_t arrayOffset; // offset to calculate the index
public:
    HFwDetStrawTrb3Lookup(const Char_t* name = "FwDetStrawTrb3Lookup",
                          const Char_t* title = "Lookup table for the TRB3 unpacker of the FwDet Straw",
                          const Char_t* context = "FwDetStrawTrb3LookupProduction",
                          Int_t minTrbnetAddress = Trbnet::kFwDetStrawTrb3MinTrbnetAddress,
                          Int_t maxTrbnetAddress = Trbnet::kFwDetStrawTrb3MaxTrbnetAddress);
    ~HFwDetStrawTrb3Lookup();

    HFwDetStrawTrb3LookupTdc* getTdc(Int_t trbnetAddress) {
        if ((arrayOffset <= trbnetAddress) && (trbnetAddress <= (array->GetSize() + arrayOffset)))
            return (HFwDetStrawTrb3LookupTdc*)(array->At(trbnetAddress - arrayOffset));
        return 0;
    }
    HFwDetStrawTrb3LookupTdc* operator[](Int_t i) {
        return static_cast<HFwDetStrawTrb3LookupTdc*>((*array)[i]);
    }

    Int_t getSize() {return array->GetLast() + 1;}
    Int_t getArrayOffset() {return arrayOffset;}

    Bool_t init(HParIo* input, Int_t* set);
    Int_t  write(HParIo* output);
    void   clear();
    void   printParams();
    Bool_t fill(Int_t, Int_t, Char_t, Char_t, Int_t, Char_t);
    Bool_t readline(const Char_t*);
    void   putAsciiHeader(TString&);
    void   write(fstream&);
    ClassDef(HFwDetStrawTrb3Lookup, 1) // Lookup table for the TRB3 unpacker of the FwDet Straw
};

#endif  /*!HFWDETSTRAWTRB3LOOKUP_H*/
