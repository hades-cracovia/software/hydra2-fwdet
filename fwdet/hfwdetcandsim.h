#ifndef HFWDETCANDSIM_H
#define HFWDETCANDSIM_H

#include "Rtypes.h"

#include "hfwdetcand.h"
#include "hvirtualcandsim.h"
#include "haddef.h"
#include "fwdetdef.h"

#include "TBits.h"
#include "TLorentzVector.h"
#include "TMatrixDSym.h"
#include "TObject.h"

class HFwDetCandSim: public HFwDetCand, public HVirtualCandSim
{
private:
    // sim info
    Int_t      fGeantTrackRpc;          // GEANT track number in FwDet RPC

    SmallFloat fGeantPx1;               // momentum of the hit in the first straw
    SmallFloat fGeantPy1;
    SmallFloat fGeantPz1;

    SmallFloat fGeantPx2;               // momentum of the hit in the last straw
    SmallFloat fGeantPy2;
    SmallFloat fGeantPz2;

    SmallFloat fGeantX1;                // position of the hit in the first straw
    SmallFloat fGeantY1;
    SmallFloat fGeantZ1;

    SmallFloat fGeantX2;                // position of the hit in the last straw
    SmallFloat fGeantY2;
    SmallFloat fGeantZ2;

    Int_t fGeantTrackInds[FWDET_STRAW_MAX_VPLANES];  // hit indices in planes

public:
    HFwDetCandSim();
    virtual ~HFwDetCandSim();

    Int_t   getGeantTrackRpc() const { return fGeantTrackRpc; }
    Float_t getGeantPx1() const { return fGeantPx1; }
    Float_t getGeantPy1() const { return fGeantPy1; }
    Float_t getGeantPz1() const { return fGeantPz1; }
    Float_t getGeantPx2() const { return fGeantPx2; }
    Float_t getGeantPy2() const { return fGeantPy2; }
    Float_t getGeantPz2() const { return fGeantPz2; }
    Float_t getGeantX1() const  { return fGeantX1; }
    Float_t getGeantY1() const  { return fGeantY1; }
    Float_t getGeantZ1() const  { return fGeantZ1; }
    Float_t getGeantX2() const  { return fGeantX2; }
    Float_t getGeantY2() const  { return fGeantY2; }
    Float_t getGeantZ2() const  { return fGeantZ2; }

    void setGeantTrackRpc(Int_t a) { fGeantTrackRpc = a; }
    void setGeantPx1(Float_t px)   { fGeantPx1 = px; }
    void setGeantPy1(Float_t py)   { fGeantPy1 = py; }
    void setGeantPz1(Float_t pz)   { fGeantPz1 = pz; }
    void setGeantPx2(Float_t px)   { fGeantPx2 = px; }
    void setGeantPy2(Float_t py)   { fGeantPy2 = py; }
    void setGeantPz2(Float_t pz)   { fGeantPz2 = pz; }
    void setGeantX1(Float_t x)     { fGeantX1 = x; }
    void setGeantY1(Float_t y)     { fGeantY1 = y; }
    void setGeantZ1(Float_t z)     { fGeantZ1 = z; }
    void setGeantX2(Float_t x)     { fGeantX2 = x; }
    void setGeantY2(Float_t y)     { fGeantY2 = y; }
    void setGeantZ2(Float_t z)     { fGeantZ2 = z; }

    inline Float_t getGeantTx() const;
    inline Float_t getGeantTy() const;

    inline Int_t addGeantHitTrack(Int_t track);

    void print(UChar_t mask = 0xff) const;

    Int_t calcGeantCorrTrackIds();

private:

    ClassDef(HFwDetCandSim, 2);
};

Float_t HFwDetCandSim::getGeantTx() const
{
    // Calculates simulated Tx slope
    Float_t dz = fGeantZ2 - fGeantZ1;
    Float_t dx = fGeantX2 - fGeantX1;
    return dx/dz;
}

Float_t HFwDetCandSim::getGeantTy() const
{
    // Calculates simulated Ty slope
    Float_t dz = fGeantZ2 - fGeantZ1;
    Float_t dy = fGeantY2 - fGeantY1;
    return dy/dz;
}

Int_t HFwDetCandSim::addGeantHitTrack(Int_t track)
{
    Int_t fNhits = getNofHits();
    if (!fNhits)
        return 0;

    fGeantTrackInds[fNhits-1] = track;
    return fNhits;
}

#endif
