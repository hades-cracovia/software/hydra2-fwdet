#ifndef HFWDETVECTORFINDERPAR_H
#define HFWDETVECTORFINDERPAR_H

#include "hparcond.h"

#include <TArrayF.h>

class HFwDetVectorFinderPar : public HParCond
{
protected:
    Int_t   nMaxAllowedHits;  // reject events with more hits in FwDet
    Float_t fMatchR;        // cut parameter for vectors distance match
    Float_t fMatchCross;    // cut parameter for vectors cross match
    Float_t fCutX;          // cut parameter for X-cord
    Float_t fCutY;          // cut parameter for Y-cord
    Float_t fLRCutChi2;     // cut parameter for low-res Chi2
    Float_t fHRCutChi2;     // cut parameter for high-res Chi2
    Float_t fTanCut;        // cut parameter for Tangens cut
    Float_t fTxyCut;        // cut parameter for Tx,y vector matching
    Float_t fLRErrU;        // low-res error of U-coordinate
    Float_t fHRErrU;        // high-res error of U-coordinate
    Int_t   nMaxBest;       // maximum best combinations for a single track
    Int_t   nPass;          // number of reco passess
    TArrayF fDriftRadiusPars;   // params for time->radius conversion

public:
    HFwDetVectorFinderPar(const Char_t * name = "VectorFinderPar",
                               const Char_t * title = "VectorFinder parameters",
                               const Char_t * context = "VectorFinderParProduction");
    virtual ~HFwDetVectorFinderPar() {}

    void    clear();
    void    putParams (HParamList *);
    Bool_t  getParams (HParamList *);

    Int_t    getMaxAllowedHits() const { return nMaxAllowedHits; }
    Float_t  getMatchR() const { return fMatchR; }
    Float_t  getMatchCross() const { return fMatchCross; }
    Float_t  getCutX() const { return fCutX; }
    Float_t  getCutY() const { return fCutY; }
    Float_t  getLRCutChi2() const { return fLRCutChi2; }
    Float_t  getHRCutChi2() const { return fHRCutChi2; }
    Float_t  getTanCut() const { return fTanCut; }
    Float_t  getTxyCut() const { return fTxyCut; }
    Float_t  getLRErrU() const { return fLRErrU; }
    Float_t  getHRErrU() const { return fHRErrU; }
    Int_t    getMaxBest() const { return nMaxBest; }
    Int_t    getNpass() const { return nPass; }
    Float_t  getDriftRadiusPar(Int_t idx) const;

    void setMaxAllowedHits(Int_t n) { nMaxAllowedHits = n; }
    void setMatchR(Float_t r) { fMatchR = r; }
    void setMatchCross(Float_t cross) { fMatchCross = cross; }
    void setCutX(Float_t cutx) { fCutX = cutx; }
    void setCutY(Float_t cuty) { fCutY = cuty; }
    void setLRCutChi2(Float_t chi2) { fLRCutChi2 = chi2; }
    void setHRCutChi2(Float_t chi2) { fHRCutChi2 = chi2; }
    void setTanCut(Float_t cut) { fTanCut = cut; }
    void setTxyCut(Float_t cut) { fTxyCut = cut; }
    void setLRErrU(Float_t err) { fLRErrU = err; }
    void setHRErrU(Float_t err) { fHRErrU = err; }
    void setMaxBest(Int_t nbest) { nMaxBest = nbest; }
    void setNpass(Int_t npass) { nPass = npass; }
    void setDriftRadiusPar(Int_t idx, Float_t p);

private:
    static const Int_t dt_pars_num = 5;

    ClassDef (HFwDetVectorFinderPar, 1);
};

#endif  /*!HFWDETVECTORFINDERPAR_H*/
