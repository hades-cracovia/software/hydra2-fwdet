//*-- AUTHOR : Ilse Koenig
//*-- Created : 16/11/2015

//_HADES_CLASS_DESCRIPTION
//_HADES_CLASS_DESCRIPTION
/////////////////////////////////////////////////////////////////////////////////
// HFwDetParAsciiFileIo
//
// Class for parameter input/output from/into Ascii file for the Forward detector
//
/////////////////////////////////////////////////////////////////////////////////

#include "hades.h"
#include "hdetector.h"
#include "hspectrometer.h"
#include "hfwdetparasciifileio.h"
#include "hparset.h"
#include "hfwdetgeompar.h"
#include "hfwdetstrawtrb3lookup.h"
#include "hfwdetstrawtrb3calpar.h"
#include "hfwdetrpctrb3lookup.h"
#include "hfwdetrpctrb3calpar.h"
#include "hfwdetstrawstatuspar.h"

ClassImp(HFwDetParAsciiFileIo);

HFwDetParAsciiFileIo::HFwDetParAsciiFileIo(fstream* f) : HDetParAsciiFileIo(f)
{
    // constructor calls the base class constructor
    fName = "HFwDetParIo";
}

HFwDetParAsciiFileIo::~HFwDetParAsciiFileIo()
{
}

Bool_t HFwDetParAsciiFileIo::init(HParSet* pPar, Int_t* set)
{
    // calls the appropriate read function for the container
    const  Text_t* name=pPar->GetName();
    if (pFile)
    {
        if (0 == strncmp(name,"FwDetGeomPar", strlen("FwDetGeomPar")))
            return HDetParAsciiFileIo::read((HDetGeomPar*)pPar,set);
        if (0 == strncmp(name, "FwDetStrawTrb3Lookup", strlen("FwDetStrawTrb3Lookup")))
            return HDetParAsciiFileIo::readFile<HFwDetStrawTrb3Lookup>((HFwDetStrawTrb3Lookup*)pPar);
        if (0 == strncmp(name, "FwDetRpcTrb3Lookup", strlen("FwDetRpcTrb3Lookup")))
            return HDetParAsciiFileIo::readFile<HFwDetRpcTrb3Lookup>((HFwDetRpcTrb3Lookup*)pPar);

        if (0 == strncmp(name, "FwDetStrawTrb3Calpar", strlen("FwDetStrawTrb3Calpar")))
            return HDetParAsciiFileIo::read((HFwDetStrawTrb3Calpar*)pPar);
        if (0 == strncmp(name, "FwDetRpcTrb3Calpar", strlen("FwDetRpcTrb3Calpar")))
            return HDetParAsciiFileIo::read((HFwDetRpcTrb3Calpar*)pPar);

        Error("init(HParSet*,Int_t*)","Initialization of %s not possible from ASCII file",name);
        return kFALSE;
    }
    Error("init(HParSet*,Int_t*)","No input file open");
    return kFALSE;
}

Int_t HFwDetParAsciiFileIo::write(HParSet* pPar)
{
    // calls the appropriate write function for the container
    if (pFile)
    {
        const  Text_t* name = pPar->GetName();
        if (0 == strncmp(name, "FwDetGeomPar", strlen("FwDetGeomPar")))
            return HDetParAsciiFileIo::writeFile((HDetGeomPar*)pPar);
        if (0 == strncmp (name, "FwDetStrawTrb3Lookup", strlen ("FwDetStrawTrb3Lookup")))
            return writeFile<HFwDetStrawTrb3Lookup> ( (HFwDetStrawTrb3Lookup *) pPar);
        if (0 == strncmp (name, "FwDetRpcTrb3Lookup", strlen ("FwDetRpcTrb3Lookup")))
            return writeFile<HFwDetRpcTrb3Lookup> ( (HFwDetRpcTrb3Lookup *) pPar);

        if (0 == strncmp (name, "FwDetStrawTrb3Calpar", strlen ("FwDetStrawTrb3Calpar")))
            return HDetParAsciiFileIo::writeFile( (HTrb3Calpar *) pPar);
        if (0 == strncmp (name, "FwDetRpcTrb3Calpar", strlen ("FwDetRpcTrb3Calpar")))
            return HDetParAsciiFileIo::writeFile( (HTrb3Calpar *) pPar);


        Error("write(HParSet*)", "%s could not be written to ASCII file",name);
        return -1;
    }
    Error("write(HParSet*)", "No output file open");
    return -1;
}
