#include "hfwdetdetector.h"
#include "hfwdetstrawgeompar.h"
#include "hparamlist.h"
#include "hades.h"
#include "hspectrometer.h"

#include <iostream>
#include <vector>

using namespace std;

ClassImp(HFwDetStrawGeomPar);

HFwDetStrawGeomPar::HFwDetStrawGeomPar(const Char_t* name, const Char_t* title,
                                       const Char_t* context) :
                                       HParCond(name, title, context), nModules(0)
{
    clear();
}

HFwDetStrawGeomPar::~HFwDetStrawGeomPar()
{
}

void HFwDetStrawGeomPar::clear()
{
    for (Int_t i = 0; i < FWDET_STRAW_MAX_MODULES; ++i)
    {
        // clear all variables
        sm_mods[i].nLayers = 0;
        sm_mods[i].nStraws = 0;
        sm_mods[i].nShortIndex = 0;
        sm_mods[i].nShortWidth = 0;
        sm_mods[i].fShortOffset = 0.0;
        sm_mods[i].fStrawRadius = 0.0;
        sm_mods[i].fStrawPitch = 0.0;
        sm_mods[i].fOffsetZ = 0.0;
        sm_mods[i].fOffsetX = 0.0;
        sm_mods[i].fLayerRotation = 0;
    }
}

Int_t HFwDetStrawGeomPar::getModules() const
{
    // return number of layers in single detector
    return nModules;
}

Int_t HFwDetStrawGeomPar::getLayers(Int_t m) const
{
    // return number of layers in module 'm'
    // m -- module number
    if (m < getModules())
        return sm_mods[m].nLayers;
    else
        return -1;
}

Int_t HFwDetStrawGeomPar::getStraws(Int_t m) const
{
    // return number of straws in single layer of module 'm'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].nStraws;
    else
        return -1;
}

Int_t HFwDetStrawGeomPar::getShortIndex(Int_t m) const
{
    // return number of straws in single layer of module 'm'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].nShortIndex;
    else
        return -1;
}

Int_t HFwDetStrawGeomPar::getShortWidth(Int_t m) const
{
    // return number of straws in single layer of module 'm'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].nShortWidth;
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getShortOffset(Int_t m) const
{
    // return number of straws in single layer of module 'm'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].fShortOffset;
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getStrawRadius(Int_t m) const
{
    // return straw radius in single sublayer of layer 'l'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].fStrawRadius;
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getStrawPitch(Int_t m) const
{
    // return straw pitch in single sublayer of layer 'l'
    // l -- layer number
    if (m < getModules())
        return sm_mods[m].fStrawPitch;
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getOffsetZ(Int_t m, Int_t l, Int_t p) const
{
    // return Z-coordinate of the beginning of the sublayer 's' in layer 'l'
    // m --module number
    // l -- layer number
    // s -- sublayer number
    if (m < getModules() && l < getLayers(m) && p < FWDET_STRAW_MAX_PLANES)
        return sm_mods[m].fOffsetZ[l][p];
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getOffsetX(Int_t m, Int_t l, Int_t p) const
{
    // return X-coordinate of the beginning of the sublayer 's' in layer 'l'
    // m -- module number
    // l -- layer number
    // s -- sublayer number
    if (m < getModules() && l < getLayers(m) && p < FWDET_STRAW_MAX_PLANES)
        return sm_mods[m].fOffsetX[l][p];
    else
        return -1;
}

Float_t HFwDetStrawGeomPar::getLayerRotation(Int_t m, Int_t l) const
{
    // return transformation matrix for a layer 'l'
    // l -- layer number
    if (m < getModules() && l < getLayers(m))
        return sm_mods[m].fLayerRotation[l];
    else
        return -1;
}

void HFwDetStrawGeomPar::setModules(int m)
{
    if (m <= FWDET_STRAW_MAX_MODULES)
        nModules = m;
}

void HFwDetStrawGeomPar::setLayers(Int_t m, Int_t l)
{
    // set number of layers, this function automatically
    // resizes all depending arrays
    sm_mods[m].nLayers = l;
    sm_mods[m].fLayerRotation.Set(l);
    sm_mods[m].fOffsetZ.ResizeTo(l, FWDET_STRAW_MAX_PLANES);
    sm_mods[m].fOffsetX.ResizeTo(l, FWDET_STRAW_MAX_PLANES);
}

void HFwDetStrawGeomPar::setStraws(Int_t m, Int_t s)
{
    // set number of straws for moduke 'm'
    // m -- module number
    // s -- number of straws
    if (m < getModules())
    {
        sm_mods[m].nStraws = s;
    }
}

void HFwDetStrawGeomPar::setShortIndex(Int_t m, Int_t i)
{
    // set number of straws for moduke 'm'
    // m -- module number
    // i -- offset of short starw
    if (m < getModules())
    {
        sm_mods[m].nShortIndex = i;
    }
}

void HFwDetStrawGeomPar::setShortWidth(Int_t m, Int_t w)
{
    // set number of straws for moduke 'm'
    // m -- module number
    // w -- width of short straws section
    if (m < getModules())
    {
        sm_mods[m].nShortWidth = w;
    }
}

void HFwDetStrawGeomPar::setShortOffset(Int_t m, Float_t o)
{
    // set number of straws for moduke 'm'
    // m -- module number
    // o -- offset of short module origin to module origin
    if (m < getModules())
    {
        sm_mods[m].fShortOffset = o;
    }
}

void HFwDetStrawGeomPar::setStrawRadius(Int_t m, Float_t r)
{
    // set straw radius in each sublayer of layer 'l'
    // l -- layer number
    // r -- straw radius
    if (m < getModules())
    {
        sm_mods[m].fStrawRadius = r;
    }
}

void HFwDetStrawGeomPar::setStrawPitch(Int_t m, Float_t p)
{
    // set straws pitch in each sublayer of layer 'l'
    // l -- layer number
    // p -- straws pitch
    if (m < getModules())
    {
        sm_mods[m].fStrawPitch = p;
    }
}

void HFwDetStrawGeomPar::setOffsetZ(Int_t m, Int_t l, Int_t p, Float_t z)
{
    // set Z-coordinate of the begining of 's' sublayer in layer 'l'
    // l -- layer number
    // s -- sublater number
    // z -- z-coordinate
    if (m < getModules() && l < getLayers(m) && p < FWDET_STRAW_MAX_PLANES)
    {
        sm_mods[m].fOffsetZ[l][p] = z;
    }
}

void HFwDetStrawGeomPar::setOffsetX(Int_t m, Int_t l, Int_t p, Float_t x)
{
    // set X-coordinate of the begining of 's' sublayer in layer 'l'
    // l -- layer number
    // s -- sublater number
    // x -- z-coordinate
    if (m < getModules() && l < getLayers(m) && p < FWDET_STRAW_MAX_PLANES)
    {
        sm_mods[m].fOffsetX[l][p] = x;
    }
}

void HFwDetStrawGeomPar::setLayerRotation(Int_t m, Int_t l, Float_t r)
{
    // set lab transform for later 'l'
    // l -- layer number
    // lt -- lab transformation matrix [3x3 rot., 3-x transl.]
    if (m < getModules() && l < getLayers(m))
    {
        sm_mods[m].fLayerRotation[l] = r;
    }
}

void HFwDetStrawGeomPar::putParams(HParamList* l)
{
    // add the parameters to the list for writing
    if (!l) return;

    Int_t total_layers = 0;

    // first find total number of layers
    for (Int_t i = 0; i < nModules; ++i)
    {
        total_layers += getLayers(i);
    }

    TArrayI par_layers(nModules);
    TArrayI par_straws(nModules);
    TArrayI par_shortidx(nModules);
    TArrayI par_shortw(nModules);
    TArrayF par_shorto(nModules);

    TArrayF par_offsetX(total_layers * FWDET_STRAW_MAX_PLANES);
    TArrayF par_offsetZ(total_layers * FWDET_STRAW_MAX_PLANES);

    TArrayF par_strawR(nModules);
    TArrayF par_strawP(nModules);

    TArrayF par_layerRotation(total_layers);

    Int_t cnt_layers = 0;

    for (Int_t i = 0; i < nModules; ++i)
    {
        // get number of layers
        Int_t layers = getLayers(i);

        // set number of layers
        par_layers.SetAt(layers, i);

        par_strawR.SetAt(getStrawRadius(i), i);
        par_strawP.SetAt(getStrawPitch(i), i);

        // iterate over layers
        for (Int_t l = 0; l < layers; ++l)
        {
            for (Int_t s = 0; s < FWDET_STRAW_MAX_PLANES; ++s)
            {
                par_offsetZ.SetAt(getOffsetZ(i, l, s), 2*(cnt_layers+l) + s);
                par_offsetX.SetAt(getOffsetX(i, l, s), 2*(cnt_layers+l) + s);
            }

            par_layerRotation.SetAt(getLayerRotation(i, l), cnt_layers + l);
        }

        // set number of straws in each block
        par_straws.SetAt(getStraws(i), i);
        // set short straws values
        par_shortidx.SetAt(getShortIndex(i), i);
        par_shortw.SetAt(getShortWidth(i), i);
        par_shorto.SetAt(getShortOffset(i), i);

        cnt_layers += layers;
    }

    l->add("nModules",       nModules);
    l->add("nLayers",        par_layers);
    l->add("nStraws",        par_straws);
    l->add("nShortIndex",    par_shortidx);
    l->add("nShortWidth",    par_shortw);
    l->add("fShortOffset",   par_shorto);
    l->add("fOffsetZ",       par_offsetZ);
    l->add("fOffsetX",       par_offsetX);
    l->add("fStrawRadius",   par_strawR);
    l->add("fStrawPitch",    par_strawP);
    l->add("fLayerRotation", par_layerRotation);
}

Bool_t HFwDetStrawGeomPar::getParams(HParamList* l)
{
    // gets the parameters from the list (read from input)
    if (!l) return kFALSE;

    Int_t par_modules;
    if (!l->fill("nModules", &par_modules))
        return kFALSE;

    TArrayI par_layers(par_modules);
    if (!l->fill("nLayers", &par_layers))
        return kFALSE;

    if (par_layers.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of layers does not fit to number of detectors");
        return kFALSE;
    }

    Int_t total_layers = 0;
    for (Int_t d = 0; d < par_modules; ++d)
    {
        total_layers += par_layers[d];
    }

    TArrayI par_straws;
    if (!l->fill("nStraws", &par_straws))
        return kFALSE;

    if (par_straws.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of straws does not fit to number of detectors");
        return kFALSE;
    }

    TArrayI par_shortidx;
    if (!l->fill("nShortIndex", &par_shortidx))
        return kFALSE;

    if (par_shortidx.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of short straws offset does not fit to number of detectors");
        return kFALSE;
    }

    TArrayI par_shortw;
    if (!l->fill("nShortWidth", &par_shortw))
        return kFALSE;

    if (par_shortw.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of short straws section width does not fit to number of detectors");
        return kFALSE;
    }

    TArrayF par_shorto;
    if (!l->fill("fShortOffset", &par_shorto))
        return kFALSE;

    if (par_shorto.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of short straws section offset does not fit to number of detectors");
        return kFALSE;
    }

    TArrayF par_strawRadius;
    if (!l->fill("fStrawRadius", &par_strawRadius))
        return kFALSE;

    if (par_strawRadius.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of strawRadius=%d does not fit to number of layers=%d", par_strawRadius.GetSize(), par_modules);
        return kFALSE;
    }

    TArrayF par_strawPitch;
    if (!l->fill("fStrawPitch", &par_strawPitch))
        return kFALSE;

    if (par_strawPitch.GetSize() != par_modules)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of strawPitch=%d does not fit to number of layers=%d", par_strawPitch.GetSize(), par_modules);
        return kFALSE;
    }

    Int_t cnt_layers = 0;
    for (Int_t d = 0; d < par_modules; ++d)
    {
        cnt_layers += par_layers[d];
    }
    const Int_t cnt_planes = cnt_layers * FWDET_STRAW_MAX_PLANES;

    TArrayF par_offsetZ(cnt_planes);
    if (!l->fill("fOffsetZ", &par_offsetZ))
        return kFALSE;

    if (par_offsetZ.GetSize() != cnt_planes)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of planeZ=%d does not fit to number of planes=%d", par_offsetZ.GetSize(), cnt_planes);
        return kFALSE;
    }

    TArrayF par_offsetX(cnt_planes);
    if (!l->fill("fOffsetX", &par_offsetX))
        return kFALSE;

    if (par_offsetX.GetSize() != cnt_planes)
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of planeX=%d does not fit to number of planes=%d", par_offsetX.GetSize(), cnt_planes);
        return kFALSE;
    }

    TArrayF par_layerRotation(total_layers);
    if (!l->fill("fLayerRotation", &par_layerRotation))
        return kFALSE;

    if (par_layerRotation.GetSize() != (Int_t)(total_layers))
    {
        Error("HFwDetStrawGeomPar::getParams(HParamList* l)",
              "Array size of layerRotation=%d does not fit to number of layers=%d", par_layerRotation.GetSize(), total_layers);
        return kFALSE;
    }

    cnt_layers = 0;

    setModules(par_modules);

    for (Int_t i = 0; i < par_modules; ++i)
    {
        // get number of layers
        Int_t layers = par_layers[i];

        // set number of layers
        setLayers(i, layers);

        setStrawRadius(i, par_strawRadius[i]);
        setStrawPitch(i, par_strawPitch[i]);

        // iterate over layers
        for (Int_t l = 0; l < layers; ++l)
        {
            for (Int_t s = 0; s < FWDET_STRAW_MAX_PLANES; ++s)
            {
                setOffsetZ(i, l, s, par_offsetZ[2*(cnt_layers + l) + s]);
                setOffsetX(i, l, s, par_offsetX[2*(cnt_layers + l) + s]);
            }
            setLayerRotation(i, l, par_layerRotation[cnt_layers + l]);
        }

        // set number of straws in each block
        setStraws(i, par_straws[i]);
        // set short straws properties
        setShortIndex(i, par_shortidx[i]);
        setShortWidth(i, par_shortw[i]);
        setShortOffset(i, par_shorto[i]);

        cnt_layers += layers;
    }

    return kTRUE;
}
