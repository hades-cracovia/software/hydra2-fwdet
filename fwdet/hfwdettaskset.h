#ifndef HFWDETTASKSET_H
#define HFWDETTASKSET_H

#include "htaskset.h"
#include "TString.h"

class HFwDetTaskSet : public HTaskSet
{
protected:
    Bool_t doStrawCal;      // calibrater (exp), digitizer (sim)
    Bool_t doRpcCal;        // calibrater (exp), digitizer (sim)
    Bool_t doRpcHitF;
    void parseArguments(TString s);

public:
    HFwDetTaskSet();
    HFwDetTaskSet(const Text_t name[], const Text_t title[]);
    virtual ~HFwDetTaskSet();
    HTask *make(const Char_t* select = "real", const Option_t* option = "strawcal,rpccal,rpchit");

    ClassDef(HFwDetTaskSet, 0); // Set of tasks
};

#endif /* !HFWDETTASKSET_H */
