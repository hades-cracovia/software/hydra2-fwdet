#ifndef HFWDETSTRAWCALRUNPAR_H
#define HFWDETSTRAWCALRUNPAR_H

#include "hparcond.h"

#include "TArrayI.h"
#include "TArrayF.h"

#include <map>

typedef std::map<Int_t, Int_t> FebID2IdxMap;

class HFwDetStrawCalRunPar : public HParCond
{
protected:
    // Parameters needed for calibration of straws per run
    Int_t nDataNum;
    TArrayF fOffset;

public:
    HFwDetStrawCalRunPar(const Char_t* name = "FwDetStrawCalRunPar",
                       const Char_t* title = "CalRun parameters for Forward Straw Detector",
                       const Char_t* context = "FwDetCalRunProduction");
    virtual ~HFwDetStrawCalRunPar();

    Int_t getDataNum() const { return nDataNum; }
    Float_t getOffset(Int_t n) const { return (n < nDataNum) ? fOffset[n] : 0.0; }

    void setDataNum(Int_t n) { nDataNum = n; fOffset.Set(n); }
    void setOffset(Int_t n, Float_t o) { if (n < nDataNum) fOffset[n] = o; }

    void   putParams(HParamList*);
    Bool_t getParams(HParamList*);
    void   clear();

    ClassDef(HFwDetStrawCalRunPar, 1); // Container for the Forward Straw Detector cal run pars
};

#endif  /* !HFWDETSTRAWCALRUNPAR_H */
