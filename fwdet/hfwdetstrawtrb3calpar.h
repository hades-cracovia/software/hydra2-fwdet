#ifndef HFWDETSTRAWTRB3CALPAR_H
#define HFWDETSTRAWTRB3CALPAR_H

#include "htrb3calpar.h"

class HFwDetStrawTrb3Calpar : public HTrb3Calpar {
public:
    HFwDetStrawTrb3Calpar(const Char_t* name = "FwDetStrawTrb3Calpar",
                      const Char_t* title = "TRB3 TDC calibration parameters of the FwDet-Straw detector",
                      const Char_t* context = "FwDetStrawTrb3CalparProduction",
                      Int_t minTrbnetAddress = Trbnet::kFwDetStrawTrb3MinTrbnetAddress,
                      Int_t maxTrbnetAddress = Trbnet::kFwDetStrawTrb3MaxTrbnetAddress)
    : HTrb3Calpar(name,title,context,minTrbnetAddress,maxTrbnetAddress) {}
    Bool_t init(HParIo* input, Int_t* set);
    Int_t write(HParIo* output);
    ClassDef(HFwDetStrawTrb3Calpar, 1) // START2 TRB3 TDC calibration parameters
};

#endif  /*!HFWDETSTRAWTRB3CALPAR_H*/
