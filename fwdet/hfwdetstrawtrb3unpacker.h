#ifndef HFWDETSTRAWTRB3UNPACKER_H
#define HFWDETSTRAWTRB3UNPACKER_H

#include "hlocation.h"
#include "htrb3tdcunpacker.h"

class HFwDetStrawTrb3Lookup;

class HFwDetStrawTrb3Unpacker: public HTrb3TdcUnpacker
{
protected:
    HLocation loc;                  // location of raw cell object
    HFwDetStrawTrb3Lookup* lookup;  // TRB3 lookup table
    Bool_t timeRef;                 // use reference time ?
    static Bool_t disableINLcor;
    
public:
    HFwDetStrawTrb3Unpacker(UInt_t id = 0);
    virtual ~HFwDetStrawTrb3Unpacker(void) {}

    virtual Bool_t init(void);
    virtual Bool_t reinit(void);
    virtual Int_t execute(void);

    void disableTimeRef(void) { timeRef = kFALSE ; }
    static void disableINL() { disableINLcor = kTRUE; }

    ClassDef(HFwDetStrawTrb3Unpacker, 0) // TRB3 unpacker for the Forward Detector
};

#endif /* !HFWDETSTRAWTRB3UNPACKER_H */
