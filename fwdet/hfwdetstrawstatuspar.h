#ifndef HFWDETSTRAWSTATUSPAR_H
#define HFWDETSTRAWSTATUSPAR_H

#include "hparcond.h"
#include "hdetpario.h"

#include "TArrayC.h"
#include "TArrayI.h"

#include "fwdetdef.h"

class HFwDetStrawStatusPar : public HParCond
{
protected:
    // Parameters needed for digitizations of straws
    Int_t   nStraws;          // number of febs
    TArrayC nStrawStatusM;    //! FEB status: mod
    TArrayC nStrawStatusL;    //! FEB status: lay
    TArrayI nStrawStatusS;    //! FEB status: straw
    TArrayC nStrawStatusC;    //! FEB status: sub-cell
    TArrayC nStrawStatus;     // FEB status: status

public:
    HFwDetStrawStatusPar(const Char_t* name = "FwDetStrawStatusPar",
                       const Char_t* title = "Straw status parameters for Forward Straw Detector",
                       const Char_t* context = "FwDetStrawStatusParProduction");
    virtual ~HFwDetStrawStatusPar();

    // variables:
    //  n - index number
    //  m - mod
    //  l - layer
    //  s - straw
    //  c - sub-cell
    //  status - status
    Int_t  getStraws() const                          { return nStraws; }
    Char_t getStrawStatus(Int_t n) const              { return (n < nStraws) ? nStrawStatus[n] : -1; }
    Char_t getStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c);

    Bool_t setStraws(Int_t n);
    Bool_t setStrawStatus(Int_t n, Char_t status);
    Bool_t setStrawStatus(Int_t n, Char_t m, Char_t l, Int_t s, Char_t c, Char_t status);

    Int_t  addStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c, Char_t status);
    Int_t  delStrawStatus(Char_t m, Char_t l, Int_t s, Char_t c);
    Int_t  delStrawStatus(Int_t n);

    void   putParams(HParamList*);
    Bool_t getParams(HParamList*);
    void   clear();

    void   printParams();

private:

    Int_t feb_status                       //! feb status
            [FWDET_STRAW_MAX_MODULES]
            [FWDET_STRAW_MAX_LAYERS]
            [FWDET_STRAW_MAX_STRAWS]
            [FWDET_STRAW_MAX_SUBCELLS];     //!  0 - general status (on), > 0 - index (0..nStraws)+1

    ClassDef(HFwDetStrawStatusPar, 1); // Container for the Forward Straw Detector digitization
};

#endif  /* !HFWDETSTRAWSTATUSPAR_H */
