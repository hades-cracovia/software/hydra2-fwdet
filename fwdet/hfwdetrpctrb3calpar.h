#ifndef HFWDETRPCTRB3CALPAR_H
#define HFWDETRPCTRB3CALPAR_H

#include "htrb3calpar.h"

class HFwDetRpcTrb3Calpar : public HTrb3Calpar {
public:
    HFwDetRpcTrb3Calpar(const Char_t* name = "FwDetRpcTrb3Calpar",
                      const Char_t* title = "TRB3 TDC calibration parameters of the FwDet-Rpc detector",
                      const Char_t* context = "FwDetRpcTrb3CalparProduction",
                      Int_t minTrbnetAddress = Trbnet::kFwDetRpcTrb3MinTrbnetAddress,
                      Int_t maxTrbnetAddress = Trbnet::kFwDetRpcTrb3MaxTrbnetAddress)
    : HTrb3Calpar(name,title,context,minTrbnetAddress,maxTrbnetAddress) {}
    Bool_t init(HParIo* input, Int_t* set);
    Int_t write(HParIo* output);
    ClassDef(HFwDetRpcTrb3Calpar, 1) // START2 TRB3 TDC calibration parameters
};

#endif  /*!HFWDETRPCTRB3CALPAR_H*/
