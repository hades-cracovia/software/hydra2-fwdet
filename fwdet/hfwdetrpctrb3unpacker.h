#ifndef HFWDETRPCTRB3UNPACKER_H
#define HFWDETRPCTRB3UNPACKER_H

#include "hlocation.h"
#include "htrb3tdcunpacker.h"

class HFwDetRpcTrb3Lookup;

class HFwDetRpcTrb3Unpacker: public HTrb3TdcUnpacker
{
protected:
    HLocation loc;                  // location of raw cell object
    HFwDetRpcTrb3Lookup* lookup;  // TRB3 lookup table
    Bool_t timeRef;             // use reference time ?
    
public:
    HFwDetRpcTrb3Unpacker(UInt_t id = 0);
    virtual ~HFwDetRpcTrb3Unpacker(void) {}
    
    Bool_t init(void);
    Int_t execute(void);

    void disableTimeRef(void) { timeRef = kFALSE ; }

    ClassDef(HFwDetRpcTrb3Unpacker, 0) // TRB3 unpacker for the Forward Detector
};

#endif /* !HFWDETRPCTRB3UNPACKER_H */
