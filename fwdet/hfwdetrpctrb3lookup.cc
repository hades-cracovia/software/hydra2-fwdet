//_HADES_CLASS_DESCRIPTION
///////////////////////////////////////////////////////////////////////
//
//  HFwDetRpcTrb3Lookup
//
//  Lookup table for the TRB3 unpacker of the FwDetRpc to map the
//  Trbnet address (range defined in htrbnetdef.h) and channel (0..255)
//  to the detector address module, strip l-r side.
//
///////////////////////////////////////////////////////////////////////

#include "hfwdetrpctrb3lookup.h"
#include "hdetpario.h"
#include "hpario.h"
#include <fstream>

using namespace std;

ClassImp(HFwDetRpcTrb3LookupChan)
ClassImp(HFwDetRpcTrb3LookupTdc)
ClassImp(HFwDetRpcTrb3Lookup)

HFwDetRpcTrb3LookupTdc::HFwDetRpcTrb3LookupTdc()
{
    // constructor creates an array of FWDETRPC_TRB3_LOOKUP_SIZE channels
    array = new TObjArray(FWDETRPC_TRB3_LOOKUP_SIZE);
    for (Int_t i = 0; i < FWDETRPC_TRB3_LOOKUP_SIZE; i++) array->AddAt(new HFwDetRpcTrb3LookupChan(), i);
}

HFwDetRpcTrb3LookupTdc::~HFwDetRpcTrb3LookupTdc()
{
    // destructor deletes the array of channels
    array->Delete();
    delete array;
}

void HFwDetRpcTrb3LookupTdc::clear()
{
    // calls the clear function for each channel
    for (Int_t i = 0; i < FWDETRPC_TRB3_LOOKUP_SIZE; i++)(*this)[i].clear();
}

//---------------------------------------------------------------

HFwDetRpcTrb3Lookup::HFwDetRpcTrb3Lookup(const Char_t* name,
                                             const Char_t* title,
                                             const Char_t* context,
                                             Int_t minTrbnetAddress,
                                             Int_t maxTrbnetAddress)
: HParSet(name, title, context)
{
    // constructor creates an empty array of size nBoards
    arrayOffset = minTrbnetAddress;
    array = new TObjArray(maxTrbnetAddress - minTrbnetAddress + 1);
}

HFwDetRpcTrb3Lookup::~HFwDetRpcTrb3Lookup()
{
    // destructor
    array->Delete();
    delete array;
}

Bool_t HFwDetRpcTrb3Lookup::init(HParIo* inp, Int_t* set)
{
    // initializes the container from an input
    HDetParIo* input = inp->getDetParIo("HFwDetParIo");
    if (input) return (input->init(this, set));
    return kFALSE;
}

Int_t HFwDetRpcTrb3Lookup::write(HParIo* output)
{
    // writes the container to an output
    HDetParIo* out = output->getDetParIo("HFwDetParIo");
    if (out) return out->write(this);
    return -1;
}

void HFwDetRpcTrb3Lookup::clear()
{
    // deletes all HFwDetRpcTrb3LookupTdc objects from the array and resets the input versions
    array->Delete();
    status = kFALSE;
    resetInputVersions();
}

void HFwDetRpcTrb3Lookup::printParams()
{
    // prints the lookup table
    printf("Lookup table for the TRB3 unpacker of the FwDetRpc\n");
    printf("trbnet-address  channel  ->  module  layer  strip  lrside\n");
    for (Int_t i = 0; i <= array->GetLast(); i++) {
        HFwDetRpcTrb3LookupTdc* b = (*this)[i];
        if (b)
        {
            for (Int_t j = 0; j < b->getSize(); j++)
            {
                HFwDetRpcTrb3LookupChan& chan = (*b)[j];
                Int_t module = chan.getModule();
                if (module >= 0)
                {
                    printf("0x%x %4i %5i %5i %5i %5i\n",
                           arrayOffset + i, j, module, chan.getLayer(), chan.getStrip(), chan.getLRside());
                }
            }
        }
    }
}

Bool_t HFwDetRpcTrb3Lookup::fill(Int_t id, Int_t chan,
                                   Char_t mod, Char_t layer, Char_t strip, Char_t lrside)
{
    // creates the HFwDetRpcTrb3LookupTdc objects, if not existing, and fills the channel
    Bool_t rc = kFALSE;
    HFwDetRpcTrb3LookupTdc* p = getTdc(id);
    if (!p)
    {
        p = new HFwDetRpcTrb3LookupTdc();
        array->AddAt(p, id - arrayOffset);
    }
    HFwDetRpcTrb3LookupChan* c = p->getChannel(chan);
    if (c)
    {
        c->fill(mod, layer, strip, lrside);
        rc = kTRUE;
    }
    else
    {
        Error("fill", "Invalid channel number %i", chan);
    }
    return rc;
}

Bool_t HFwDetRpcTrb3Lookup::readline(const Char_t *buf)
{
    // decodes one line read from ASCII file I/O and fills the channel
    Bool_t rc = kFALSE;
    Int_t id, chan, mod, lay, strip, lrside;
    Int_t n = sscanf(buf, " 0x%x %i %i %i %i %i", &id, &chan, &mod, &lay, &strip, &lrside);
    if (6 == n)
    {
        rc = fill(id, chan, mod, lay, strip, lrside);
    }
    else
    {
        if (n < 6) Error("readline", "Not enough values in line %s\n", buf);
        else     Error("readline", "Too many values in line %s\n", buf);
    }
    return rc;
}

void HFwDetRpcTrb3Lookup::putAsciiHeader(TString& header)
{
    // puts the ASCII header to the string used in HFwDetRpcParAsciiFileIo
    header = "# Lookup table for the TRB3 unpacker of the FwDetRpc\n"
    "# Format:\n"
    "# trbnet-address  channel  module  layer  strip  lrside\n";
}

void HFwDetRpcTrb3Lookup::write(fstream& fout)
{
    // writes the information of all non-zero HFwDetRpcTrb3LookupTdc objects to the ASCII file
    for (Int_t i = 0; i <= array->GetLast(); i++)
    {
        HFwDetRpcTrb3LookupTdc* b = (*this)[i];
        if (b)
        {
            for (Int_t j = 0; j < b->getSize(); j++)
            {
                HFwDetRpcTrb3LookupChan& chan = (*b)[j];
                Int_t module = chan.getModule();
                if (module >= 0)
                {
                    fout << "0x" << hex << (arrayOffset + i) << dec << setw(5) << j
                    << setw(5) << module << setw(5) << chan.getLayer()
                    << setw(5) << chan.getStrip() << setw(5) << chan.getLRside() << '\n';
                }
            }
        }
    }
}
