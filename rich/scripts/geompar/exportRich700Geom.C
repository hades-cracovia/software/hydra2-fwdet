{
  // create a Hades object and get the
  // pointers to the runtime database and
  // the spectrometer
  Hades* myHades = new Hades;
  HSpectrometer* spec = gHades->getSetup();
  HRuntimeDb* rtdb = gHades->getRuntimeDb(); 
 
  // create the detector and its setup
  // and add it in the spectrometer
  HRichDetector* rich = new HRichDetector;
  Int_t mods[] = {1};
  rich->setModules(-1,mods);
  spec->addDetector(rich);
 
  // define the input
  HParOra2Io* ora = new HParOra2Io;
  //ora->open();
  ora->open("db-hades-test","rich_ana2");
  ora->setHistoryDate("now");
  rtdb->setFirstInput(ora);

  // define the output
//   HParRootFileIo* output = new HParRootFileIo;
//   output->open("Rich700Params.root","RECREATE");
//   rtdb->setOutput(output);
  
   HParAsciiFileIo* output = new HParAsciiFileIo;
  output->open("Rich700Geometry4.txt","out");
  rtdb->setOutput(output);

  // create the parameter containers
  HParSet* mypar=rtdb->getContainer("Rich700GeomPar");
  if(mypar)
  {
     std::cout <<"found Rich700GeomPar parameter:" << std::endl;
    mypar->Print();    
  }
  else
  {
  
      std::cout <<"NOT found Rich700GeomPar parameter:" << std::endl;
     delete myHades;
     return;    
  }
  
  rtdb->initContainers(275840519);// recorded cosmics to testdb in november 2017
  //rtdb->initContainers(1); // the latest default?
  // save the output
  // (writes the data to the file)
  rtdb->saveOutput();

  // print content of the runtime
  // database on the screen
  rtdb->print();

  
  // delete the Hades object
  delete myHades;
} 
